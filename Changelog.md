## 10.0.1
* First v10 release

## 1.0.6
* Remains last v9 compatible version

## 1.0.4
* Fix for breaking calendar-weather moons calculation.

## 1.0.3
* Fix for calendar initialisation failing.
* Fix for pathfinder 2e initialisation.

## 1.0.2
* Ability to disable the myriad of warning messages - via config setting debug.
* Gate attempting to set worldTime to a non-integer value.

## 1.0.1
Recognising that there is no good reason to support more than one calendar management solution (especially since Simple Calendar does a MUCH better job of that than about-time) and that it benefits the community to have modules play nicely together I've decided to switch about-time to use the Simple Calendar module for calendar and time management.

[BREAKING BREAKING] 
* about-time now depends on Simple Calendar to operate.
* about-time no longer manages calendars at all
* about-time no loner advances the game clock.

* Changes to calendar-weather calendar are automatically pushed to simple calendar.
* Changes made to Simple Calendar still have to be exported to calendar-weather via Simple Calendar

Calendar config settings.
* To migrate, load your about-time userdefind calendar (if there is one) into Simple Calendar from the Simple Calendar config settings.
* Set the real time interval in Simple Calendr to match those from about-time.

There are extensive notes in the about-time Readme.md, please read them.
Going forwards about-time will solely manage event scheduling and effect expiry.

## 0.8.5
* fix for ordinary users trying to update the game settings on load.

## 0.8.4
* about-time should automatically synchronise to the pf2e world time and stay in step once loaded. You don't need to set any offset fields, the pf2e world creation date/time is used by about-time.

## 0.8.3
* correct offset for m/d in zero time offset.

## 0.8.2
* Updated various language translations as well, thanks to @klo, benwater12 and Jose Lazano.

## 0.8.1
* Added World Time 0 calendar date/time offset. This allows you to specify an arbitrary game time that corresponds to game.time.worldtime = 0, format is y/m/d h:m:s. Should help with PF2E date/time problems.

## 0.8.0
Some html cleanup.
abut-time seems to be 0.8.x compatible.

## 0.1.68
* Added named years support. Internally all calculations are done with numeric years. On output a given year number is converted to a year name assuming year names cycle starting at 0.
* Added support for year names in calendar editor.
* See Readme.md for example usage.
* Added sample calendar's for Dark Sun and Nehwon.
* Updated es.json - thanks Jose Lozano

